import axios from 'axios';

const API_KEY = '5d34405e43b6d287f01121bfda249296';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WAETHER = 'FETCH_WAETHER';

export function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city},us`;
  const request = axios.get(url);

  return {
    type: FETCH_WAETHER,
    payload: request
  };
}
